/**
 * Created by lakhe on 8/18/16.
 */

var clubController = (function () {

    'use strict';

    var dataProviderHelper = require('../data/mongo.provider.helper'),
        HTTPStatus = require('http-status'),
        messageConfig = require('../configs/api.message.config'),
        Club = require('../models/club.server.model'),
        utilityHelper = require('../helpers/utilities.helper'),
        errorHelper = require('../helpers/error.helper'),
        Promise = require("bluebird");

    var documentFields = '_id clubName clubInternationalID clubSponsorName clubZone linkURL imageName imageAltText active';

    function ClubModule() {}

    ClubModule.CreateClub = function (clubObj, loggedInUser, imageInfo) {
        var clubInfo = new Club();
        clubInfo.clubName = clubObj.clubName;
        clubInfo.clubInternationalID = clubObj.clubInternationalID;
        clubInfo.clubSponsorName = clubObj.clubSponsorName;
        clubInfo.clubZone = clubObj.clubZone;
        clubInfo.linkURL = clubObj.linkURL;
        clubInfo.active = clubObj.active;
        clubInfo.imageName = imageInfo._imageName;
        clubInfo.imageAltText = clubObj.imageAltText;
        clubInfo.imageProperties = {
            imageExtension: imageInfo._imageExtension,
            imagePath: imageInfo._imagePath
        };
        clubInfo.addedBy = loggedInUser;
        clubInfo.addedOn = new Date();
        return clubInfo;
    };

    var _p = ClubModule.prototype;

    _p.checkValidationErrors = function (req) {

        req.checkBody('clubName', messageConfig.club.validationErrMessage.clubName).notEmpty();
        req.checkBody('linkURL', messageConfig.club.validationErrMessage.linkURL).notEmpty();
        req.checkBody('linkURL', messageConfig.club.validationErrMessage.webURLValid).isURL();

        return req.validationErrors();
    };

    _p.getClubs = function (req, next) {
        var pagerOpts = utilityHelper.getPaginationOpts(req, next);

        var query = {};
        // matches anything that  starts with the inputted team member's name, case insensitive
        if (req.query.clubname) {
            query.clubName = {$regex: new RegExp('.*' + req.query.clubname, "i")};
        }
        if (req.query.active) {
            query.active = req.query.active;
        }
        query.deleted = false;
        var sortOpts = { addedOn: -1 };

        return dataProviderHelper.getAllWithDocumentFieldsPagination(Club, query, pagerOpts, documentFields, sortOpts);
    };

    _p.getClubById = function (req) {
        var selectFields = documentFields + ' imageProperties';
        return dataProviderHelper.findById(Club, req.params.clubId, selectFields);
    };

    _p.deleteClubInfo = function (req, res, next) {
        req.clubInfo.deleted = true;
        req.clubInfo.deletedOn = new Date();
        req.clubInfo.deletedBy = req.decoded.user.username;

        dataProviderHelper.save(req.clubInfo)
            .then(function () {
                res.status(HTTPStatus.OK);
                res.json({
                    message: messageConfig.club.deleteMessage
                });
            })
            .catch(function (err) {
                return next(err);
            });
    };

    _p.postClubInfo = function (req, res, next) {
        console.log("hi");
        req.body = JSON.parse(req.body.data);
        var errors = _p.checkValidationErrors(req);
        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        } else {
            var imageInfo = utilityHelper.getFileInfo(req, null, next);
            if (imageInfo._imageName) {
                var modelInfo = utilityHelper.sanitizeUserInput(req, next);
                var query = {};
                query.linkURL = modelInfo.linkURL.trim().toLowerCase();
                query.deleted = false;

                dataProviderHelper.checkForDuplicateEntry(Club, query)
                    .then(function (count) {
                        if (count > 0) {
                            throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.club.alreadyExists + '"}');
                        } else {
                            var clubInfo = ClubModule.CreateClub(modelInfo, req.decoded.user.username, imageInfo);
                            return dataProviderHelper.save(clubInfo);
                        }
                    })
                    .then(function () {
                        res.status(HTTPStatus.OK);
                        res.json({
                            message: messageConfig.club.saveMessage
                        });
                    })
                    .catch(Promise.CancellationError, function (cancellationErr) {
                        errorHelper.customErrorResponse(res, cancellationErr, next);
                    })
                    .catch(function (err) {
                        return next(err);
                    });
            } else {
                res.status(HTTPStatus.BAD_REQUEST);
                res.json({
                    message: messageConfig.club.fieldRequiredImage
                });
            }
        }
    };

    _p.updateClubInfo = function (req, res, next) {
        req.body = JSON.parse(req.body.data);
        var errors = _p.checkValidationErrors(req);

        if (errors) {
            res.status(HTTPStatus.BAD_REQUEST);
            res.json({
                message: errors
            });
        } else {
            var imageInfo = utilityHelper.getFileInfo(req, req.clubInfo, next);
            if (imageInfo._imageName) {
                var modelInfo = utilityHelper.sanitizeUserInput(req, next);
                var query = {};
                query.linkURL = modelInfo.linkURL.trim().toLowerCase();
                query.deleted = false;

                if (req.clubInfo.linkURL !== modelInfo.linkURL.trim().toLowerCase()) {
                    dataProviderHelper.checkForDuplicateEntry(Club, query)
                        .then(function (count) {
                            if (count > 0) {
                                throw new Promise.CancellationError('{ "statusCode":"' + HTTPStatus.CONFLICT + '", "message": "' + messageConfig.club.alreadyExists + '"}');
                            } else {
                                return _p.updateClubFunc(req, imageInfo, modelInfo, next);
                            }
                        })
                        .then(function () {
                            res.status(HTTPStatus.OK);
                            res.json({
                                message: messageConfig.club.updateMessage
                            });
                        })
                        .catch(Promise.CancellationError, function (cancellationErr) {
                            errorHelper.customErrorResponse(res, cancellationErr, next);
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                } else {
                    _p.updateClubFunc(req, imageInfo, modelInfo, next)
                        .then(function () {
                            res.status(HTTPStatus.OK);
                            res.json({
                                message: messageConfig.club.updateMessage
                            });
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                }
            } else {
                res.status(HTTPStatus.BAD_REQUEST);
                res.json({
                    message: messageConfig.club.fieldRequiredImage
                });
            }
        }
    };

    _p.updateClubFunc = function (req, imageInfo, modelInfo) {
        req.clubInfo.clubName = modelInfo.clubName;
        req.clubInfo.linkURL = modelInfo.linkURL;
        req.clubInfo.active = modelInfo.active;
        req.clubInfo.imageName = imageInfo._imageName;
        req.clubInfo.imageAltText = modelInfo.imageAltText;
        req.clubInfo.imageProperties.imageExtension = imageInfo._imageExtension;
        req.clubInfo.imageProperties.imagePath = imageInfo._imagePath;
        req.clubInfo.updatedBy = req.decoded.user.username;
        req.clubInfo.updatedOn = new Date();
        return dataProviderHelper.save(req.clubInfo)
    };

    return {
        getClubs: _p.getClubs,
        getClubById: _p.getClubById,
        deleteClubInfo: _p.deleteClubInfo,
        postClubInfo: _p.postClubInfo,
        updateClubInfo: _p.updateClubInfo
    };

})();

module.exports = clubController;