/**
 * Created by lakhe on 8/18/16.
 */

var clubRoutes = (function () {

    'use strict';

    var HTTPStatus = require('http-status'),
        express = require('express'),
        tokenAuthMiddleware = require('../middlewares/token.authentication.middleware'),
        roleAuthMiddleware = require('../middlewares/role.authorization.middleware'),
        messageConfig = require('../configs/api.message.config'),
        clubController = require('../controllers/club.server.controller'),
        imageFilePath = './public/uploads/images/clubs/',
        uploadPrefix = 'club',
        fileUploadHelper = require('../helpers/file.upload.helper')(imageFilePath, '', uploadPrefix),
        uploader = fileUploadHelper.uploader,
        clubRouter =  express.Router();

    clubRouter.route('/')
        .get( getAllClubs )
        .post( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, uploader.single('imageName'), fileUploadHelper.imageUpload, clubController.postClubInfo );


    clubRouter.use('/:clubId', function(req, res, next){
        clubController.getClubById(req)
            .then(function(clubInfo){
                if (clubInfo) {
                    req.clubInfo = clubInfo;
                    next();
                    return null;// return a non-undefined value to signal that we didn't forget to return promise
                } else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.club.notFound
                    });
                }
            })
            .catch(function(err){
                return next(err);
            });
    });


    clubRouter.route('/:clubId')
        .get(function(req, res){
            res.status(HTTPStatus.OK);
            res.json(req.clubInfo);
        })
        .patch( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, clubController.deleteClubInfo )
        .put( tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorize, uploader.single('imageName'), fileUploadHelper.imageUpload, clubController.updateClubInfo );



    function getAllClubs(req, res, next) {
        clubController.getClubs (req, next)
            .then(function(clubs){
                if (clubs) {
                    res.status(HTTPStatus.OK);
                    res.json(clubs);
                } else {
                    res.status(HTTPStatus.NOT_FOUND);
                    res.json({
                        message: messageConfig.club.notFound
                    });
                }
            })
            .catch(function(err){
                return next(err);
            });
    }

    return clubRouter;

})();

module.exports = clubRoutes;