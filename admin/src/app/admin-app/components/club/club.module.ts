import {NgModule}      from '@angular/core';
import {ClubComponent} from "./club-list.component";
import {ClubService} from"./club.service";
import {ClubEditorComponent} from"./club-editor.component";

import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [ClubComponent,ClubEditorComponent],
  providers:[ClubService]
})

export class ClubModule {
}
