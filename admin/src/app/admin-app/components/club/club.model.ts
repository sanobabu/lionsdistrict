import {ImageProperties}from "../../../shared/models/image.model";
export class ClubModel {
  constructor() {
    this.active = false;
  }
  _id:string;
  clubName:string;
  clubInternationalID:number;
  clubSponsorName:string;
  clubZone:string;  
  imageName:string;
  imageAltText:string;
  linkURL:string;
  imageProperties:ImageProperties;
  active:boolean;
  addedBy:string;
  addedOn:string;
  updatedBy:string;
  updatedOn:string;
  deleted:boolean;
  deletedBy:string;
  deletedOn:string;
}

export class ClubResponse {
  dataList:ClubModel[];
  totalItems:number;
  currentPage:number;
}
