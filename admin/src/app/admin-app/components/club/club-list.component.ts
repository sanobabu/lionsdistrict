import {Component, ElementRef, OnInit, Output, Input, EventEmitter} from '@angular/core';
import {ClubService} from "./club.service";
import{ClubResponse, ClubModel} from "./club.model";

@Component({
  selector: 'club-list',
  templateUrl: './club-list.html'
})

export class ClubComponent implements OnInit {

  objListResponse:ClubResponse = new ClubResponse();
  error:any;
  showForm:boolean = false;
  clubId:string;

  // /* Pagination */
  perPage:number = 10;
  currentPage:number = 1;
  totalPage:number = 1;
  first:number = 0;
  bindSort:boolean = false;
  preIndex:number = 0;
  // /* End Pagination */

  ngOnInit() {
    this.getClubList();
  }

  constructor(private _objService:ClubService) {
  }

  getClubList() {
    this._objService.getClubList()
      .subscribe(objRes => this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");

  }

  bindList(objRes:ClubResponse) {
    this.objListResponse = objRes;
    /* Pagination */
    this.preIndex = (this.perPage * (this.currentPage - 1));

    if (objRes.dataList.length > 0) {
      let totalPage = objRes.totalItems / this.perPage;
      this.totalPage = totalPage > 1 ? Math.ceil(totalPage) : 1;
      this.sortTable();
      /*End Pagination */

    }
  }

  sortTable() {
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          2: {sorter: false},
          3: {sorter: false}
        }
      });
    }, 50);
  }

  edit(id:string) {
    this.showForm = true;
    this.clubId = id;
  }

  addClub() {
    this.showForm = true;
    this.clubId = null;
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objClub:ClubModel = new ClubModel();
        objClub._id = id;
        objClub.deleted = true;
        this._objService.deleteClub(objClub)
          .subscribe(res=> {
              this.getClubList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }

  showClubList(arg) {
    if (!arg) // is not Canceled
      this.getClubList();
    this.showForm = false;
    this.sortTable();
  }


}

