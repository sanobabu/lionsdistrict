import {Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {ClubModel} from "./club.model";
import {ClubService} from "./club.service";
import{Config} from "../../../shared/configs/general.config";
import{ImageCanvasSizeEnum} from "../../../shared/configs/enum.config";
import {FormGroup, FormControl, Validators, FormBuilder} from "@angular/forms";
import {ValidationService} from "../../../shared/services/validation.service";

@Component({
  selector: 'club-editor',
  templateUrl: './club-editor.html'
})
export class ClubEditorComponent implements OnInit,AfterViewInit {
  objClub:ClubModel = new ClubModel();
  @Input() clubId:string;
  @Output() showClubListEvent:EventEmitter<any> = new EventEmitter();
  clubForm:FormGroup;
  isSubmitted:boolean = false;

  /* Image Upload Handle*/
  imageDeleted:boolean = false;
  file:File;
  fileName:string = "";
  drawImagePath:string = Config.DefaultImage;
  imageFormControl:FormControl = new FormControl('', Validators.required);
  canvasSize:number = ImageCanvasSizeEnum.small;
  /* End Image Upload handle */


  constructor(private _objService:ClubService, private _formBuilder:FormBuilder) {
    this.clubForm = _formBuilder.group({
      "clubName": ['', Validators.required],
      "clubInternationalID": ['', Validators.required],
      "clubSponsorName": ['', Validators.required],
      "clubZone": ['', Validators.required],
      "imageAltText": ['', Validators.required],
      "linkURL": ['', Validators.compose([Validators.required, ValidationService.urlValidator])],
      "active": [''],
      "imageFormControl": this.imageFormControl
    });
  }

  ngAfterViewInit() {
    if (!this.clubId)
      this.drawImageToCanvas(Config.DefaultImage);
  }

  ngOnInit() {
    if (this.clubId)
      this.getImageDetail();
  }

  getImageDetail() {
    this._objService.getClubDetail(this.clubId)
      .subscribe(res =>this.bindDetail(res),
        error => this.errorMessage(error));
  }

  bindDetail(objRes:ClubModel) {
    this.objClub = objRes;
    this.fileName = this.objClub.imageName;
    (<FormControl>this.clubForm.controls['imageFormControl']).patchValue(this.fileName);
    let path:string = "";
    if (this.objClub.imageName) {
      var cl = Config.Cloudinary;
      path = cl.url(this.objClub.imageName);
    }
    else
      path = Config.DefaultImage;
    this.drawImageToCanvas(path);
  }


  saveClub() {
    this.isSubmitted = true;
    (<FormControl>this.clubForm.controls['imageFormControl']).patchValue(this.fileName);

    if (this.clubForm.valid) {
      if (!this.clubId) {
        this._objService.saveClub(this.objClub, this.file)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
      else {
        this._objService.updateClub(this.objClub, this.file, this.imageDeleted)
          .subscribe(res => this.resStatusMessage(res),
            error => this.errorMessage(error));
      }
    }
  }

  resStatusMessage(objSave:any) {
    this.showClubListEvent.emit(false); // is Form Canceled
    swal("Success !", objSave.message, "success")

  }

  triggerCancelForm() {
    let isCanceled = true;
    this.showClubListEvent.emit(isCanceled);
  }

  errorMessage(objResponse:any) {
    jQuery.jAlert({
      'title': 'Alert',
      'content': objResponse.message,
      'theme': 'red'
    });
  }

  /*Image handler */
  changeFile(args) {
    this.file = args;
    if (this.file)
      this.fileName = this.file.name;
  }

  drawImageToCanvas(path:string) {
    this.drawImagePath = path;
  }

  deleteImage(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Image !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        this._objService.deleteImage(this.objClub.imageName, this.objClub.imageProperties.imageExtension, this.objClub.imageProperties.imagePath)
          .subscribe(res=> {
              this.imageDeleted = true;
              this.objClub.imageName = "";
              this.drawImageToCanvas(Config.DefaultImage);
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }


  /* End ImageHandler */
}

