import {NgModule}      from '@angular/core';
import {DesignationEditorComponent} from "./designation-editor.component";
import {DesignationComponent} from"./designation-list.component";
import {DesignationService} from './designation.service'
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  declarations: [DesignationEditorComponent,
    DesignationComponent
  ],
  providers: [DesignationService]
})
export class DesignationModule {
}
