import {DesignationModel, DesignationResponse} from './designation.model';
import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import{Config} from "../../../shared/configs/general.config";
import{API_URL} from "../../../shared/configs/env.config";
import {FileOperrationService} from '../../../shared/services/fileOperation.service';

@Injectable()
export class DesignationService {
  DesignationApiRoute:string = "designation";
  progressObserver:any;
  progress:any;

  constructor(private _http:Http, private fileService:FileOperrationService) {
    this.progress = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  /* Designation */
  saveDesignation(objDesignation:DesignationModel) {
    let body = JSON.stringify(objDesignation);
    return this._http.post(API_URL + this.DesignationApiRoute, body)
      .map(res => res.json())
      .catch(this.handleError);
  }

  updateDesignation(objDesignation:DesignationModel) {
    let body = JSON.stringify(objDesignation);
    return this._http.put(API_URL + this.DesignationApiRoute + "/" + objDesignation._id, body)
      .map(res => res.json())
      .catch(this.handleError);
  }

  getDesignationList(active?:boolean):Observable < DesignationModel[]> {
    var queryString = "";
    if (active)
      queryString = "?active=true";
    return this._http.get(API_URL + this.DesignationApiRoute + queryString)
      .map(res =><DesignationModel[]>res.json())
      .catch(this.handleError);
  }

  getDesignationDetail(objId:string):Observable < DesignationModel> {
    return this._http.get(API_URL + this.DesignationApiRoute + "/" + objId)
      .map(res =><DesignationModel>res.json())
      .catch(this.handleError);
  }

  deleteDesignation(objDel:DesignationModel):Observable<any> {
    let body = JSON.stringify({});
    return this._http.patch(API_URL + this.DesignationApiRoute + "/" + objDel._id, body)
      .map(res => res.json())
      .catch(this.handleError);
  }

  /* End Designation*/



  handleError(error) {
    console.log(error.json());
    return Observable.throw(error.json() || 'server error');
  }

}
