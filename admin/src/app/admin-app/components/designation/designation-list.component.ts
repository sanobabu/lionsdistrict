import {Component, ElementRef, OnInit, Output, Input, EventEmitter, OnChanges} from '@angular/core';
import {DesignationService} from "./designation.service";
import {DesignationModel, DesignationResponse} from "./designation.model";
import {Paginator} from 'primeng/primeng';
import {DesignationEditorComponent} from  "./designation-editor.component";

@Component({
  selector: 'designation-list',
  templateUrl: './designation-list.html'
})

export class DesignationComponent implements OnInit,OnChanges {

  objListResponse:DesignationModel[];
  error:any;
  @Input() showList:boolean;
  @Output() showFormEvent:EventEmitter<any> = new EventEmitter();
  designationId:string;
  showForm:boolean = false;
  /* Pagination */
  perPage:number = 10;
  currentPage:number = 1;
  totalPage:number = 1;
  first:number = 0;

  ngOnInit() {
    this.perPage = 10;
    this.currentPage = 1;
    //   if (!this.isCanceled)
    this.getDesignationList();
  }

  ngOnChanges() {
    if (this.showList)
      this.showForm = !this.showList;
  }

  constructor(private _objService:DesignationService) {
  }

  getDesignationList() {
    this._objService.getDesignationList()
      .subscribe(objRes =>this.bindList(objRes),
        error => this.errorMessage(error));
  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");

  }

  bindList(objRes:DesignationModel[]) {
    this.objListResponse = objRes;
    if (objRes.length > 0) {
      this.sortTable();
    }
  }

  sortTable() {
    setTimeout(()=> {
      jQuery('.tablesorter').tablesorter({
        headers: {
          2: {sorter: false},
          3: {sorter: false}
        }
      });
    }, 50);
  }

  edit(id:string) {
    //  this.showFormEvent.emit(id);
    this.showForm = true;
    this.designationId = id;
  }

  addDesignation() {
    // this.showFormEvent.emit(null);
    this.showForm = true;
    this.designationId = null;
  }

  showDesignationList(args) {
    if (!args) // is Cancelled
      this.getDesignationList();
    this.showForm = false;
    this.sortTable();
  }

  delete(id:string) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this  Designation !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      ()=> {
        let objTemp:DesignationModel = new DesignationModel();
        objTemp._id = id;
        objTemp.deleted = true;
        this._objService.deleteDesignation(objTemp)
          .subscribe(res=> {
              this.getDesignationList();
              swal("Deleted!", res.message, "success");
            },
            error=> {
              swal("Alert!", error.message, "info");

            });
      });

  }
  vppChanged(event:Event) {
    this.perPage = Number((<HTMLSelectElement>event.srcElement).value);
    this.getDesignationList();
  }

  pageChanged(arg) {

    this.currentPage = arg;
    this.getDesignationList();

  }
}

