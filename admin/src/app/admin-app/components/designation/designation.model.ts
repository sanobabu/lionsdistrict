import {ImageModel} from "../../../shared/models/image.model";
export class DesignationModel {
  constructor() {
    this.active = false;
    this.allowMultiple = false;
  }
  _id:string;
  designationName:string;
  allowMultiple:boolean;
  active:boolean;
  addedBy:string;
  addedOn:string;
  updatedBy:string;
  updatedOn:string;
  deleted:boolean;
  deletedBy:string;
  deletedOn:string;
}
export class DesignationResponse {
  dataList:DesignationModel[];
  totalItems:number;
  currentPage:number;
}
