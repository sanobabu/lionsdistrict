import {Component, EventEmitter, Output, Input, AfterViewInit, OnInit} from '@angular/core';
import {FormControlMessages} from "../../../shared/components/control-valdation-message.component";
import {DesignationModel, DesignationResponse } from "./designation.model";
import {DesignationService} from "./designation.service";
import {FormGroup, Validators, FormBuilder,} from "@angular/forms";

@Component({
  selector: 'designation-editor',
  templateUrl: './designation-editor.html'
})


export class DesignationEditorComponent implements OnInit {
  objDesignation:DesignationModel = new DesignationModel();
  designationForm:FormGroup;
  isSubmitted:boolean = false;
  @Input() designationId:string;
  @Output() showListEvent:EventEmitter<any> = new EventEmitter();


  constructor(private _objService:DesignationService, private _formBuilder:FormBuilder) {
    this.designationForm = this._formBuilder.group({
        "designationName": ['', Validators.required],
        "allowMultiple":[''],
        "active": ['']
      }
    );
  }

  ngOnInit() {
    if (this.designationId)
      this.getDesignationDetail();
  }

  getDesignationDetail() {
    this._objService.getDesignationDetail(this.designationId)
      .subscribe(res => {
          this.objDesignation = res;
        },
        error => this.errorMessage(error));
  }


  saveDesignation() {
    this.isSubmitted = true;
    if (this.designationForm.valid) {
      if (!this.designationId) {
        this._objService.saveDesignation(this.objDesignation)
          .subscribe(res => this.resStatusMessage(res),
            error =>this.errorMessage(error));
      }
      else {
        this._objService.updateDesignation(this.objDesignation)
          .subscribe(res => this.resStatusMessage(res),
            error =>this.errorMessage(error));
      }
    }
  }

  resStatusMessage(res:any) {
    this.showListEvent.emit(false); // * isCanceled = false
    swal("Success !", res.message, "success")

  }

  errorMessage(objResponse:any) {
    swal("Alert !", objResponse.message, "info");

  }

  triggerCancelForm() {
    let isCanceled = true;
    this.showListEvent.emit(isCanceled);
  }


}

